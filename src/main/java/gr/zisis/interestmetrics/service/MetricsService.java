package gr.zisis.interestmetrics.service;

import java.util.Collection;

import gr.zisis.interestmetrics.domain.Metrics;

/**
 * @author George Digkas <digasgeo@gmail.com>
 *
 */
public interface MetricsService {

	Collection<Metrics> findAll();

	Metrics findOne(Integer id);

}
