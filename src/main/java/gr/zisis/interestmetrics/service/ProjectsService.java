package gr.zisis.interestmetrics.service;

import java.util.Collection;

import gr.zisis.interestmetrics.domain.Projects;

/**
 * @author George Digkas <digasgeo@gmail.com>
 *
 */
public interface ProjectsService {

	Collection<Projects> findAll();

	Projects findOne(Integer id);

}
