package gr.zisis.interestmetrics.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import gr.zisis.interestmetrics.domain.Metrics;
import gr.zisis.interestmetrics.persistence.MetricsRepository;

/**
 * @author George Digkas <digasgeo@gmail.com>
 *
 */
@Service
public class MetricsServiceBean implements MetricsService {

	@Autowired
	private MetricsRepository metricsRepository;

	@Override
	public Collection<Metrics> findAll() {
		return metricsRepository.findAll();
	}

	@Override
	public Metrics findOne(Integer id) {
		return metricsRepository.findById(id).get();
	}

}
