package gr.zisis.interestmetrics.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import gr.zisis.interestmetrics.domain.Projects;
import gr.zisis.interestmetrics.persistence.ProjectsRepository;

/**
 * @author George Digkas <digasgeo@gmail.com>
 *
 */
@Service
public class ProjectsServiceBean implements ProjectsService {

	@Autowired
	private ProjectsRepository projectsRepository;

	@Override
	public Collection<Projects> findAll() {
		return projectsRepository.findAll();
	}

	@Override
	public Projects findOne(Integer id) {
		return projectsRepository.findById(id).get();
	}

}
