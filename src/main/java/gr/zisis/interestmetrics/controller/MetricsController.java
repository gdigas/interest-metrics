/**
 * 
 */
package gr.zisis.interestmetrics.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author George Digkas <digasgeo@gmail.com>
 *
 */
@RestController
@RequestMapping(value = "/api/metrics")
public class MetricsController {

}
