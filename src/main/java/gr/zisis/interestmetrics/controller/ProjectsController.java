package gr.zisis.interestmetrics.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import gr.zisis.interestmetrics.domain.Projects;
import gr.zisis.interestmetrics.service.ProjectsService;

/**
 * @author George Digkas <digasgeo@gmail.com>
 *
 */
@RestController
@RequestMapping(value = "/api/projects")
public class ProjectsController {

	@Autowired
	private ProjectsService projectsService;

	@CrossOrigin(origins = "*")
	@GetMapping(value = "/findAll", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Collection<Projects>> findAll() {

		Collection<Projects> response = projectsService.findAll();

		return new ResponseEntity<Collection<Projects>>(response, HttpStatus.OK);
	}

	@CrossOrigin(origins = "*")
	@GetMapping(value = "/findOne", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Projects> findOne(@RequestParam(value = "id", required = true) int id) {

		Projects response = projectsService.findOne(id);

		return new ResponseEntity<Projects>(response, HttpStatus.OK);
	}

}
